import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import store from './store'
import './registerServiceWorker'
import {routes} from "./routes";
import Donut from 'vue-css-donut-chart';
import 'vue-css-donut-chart/dist/vcdonut.css';
import Button from './UI/Button.vue'
import Input from './UI/Input.vue'

Vue.use(Donut);

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes
});

Vue.component('sw-button', Button);
Vue.component('sw-input', Input);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
