import Profile from './components/profile/Profile.vue'
import News from './components/news/News.vue'
import Statistics from './components/statistics/Statistics.vue'
import Bookmark from './components/bookmark/Bookmark.vue'
import Notification from './components/notification/Notification.vue'


export const routes = [
    { path: '', component: Profile },
    { path: '/profile', component: Profile },
    { path: '/news', component: News },
    { path: '/statistics', component: Statistics },
    { path: '/bookmark', component: Bookmark },
    { path: '/notification', component: Notification },

];
